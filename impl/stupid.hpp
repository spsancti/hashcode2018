#include "../libs/DataStructure.hpp"
#include <algorithm>

void solve(Data& data)
{
    std::vector<size_t> rides_list;
    for (size_t i = 0; i < data.rides.size(); ++i)
        rides_list.push_back(i);

    std::random_shuffle(rides_list.begin(), rides_list.end());
    int ride_idx = rides_list.size() - 1;
    while (ride_idx >= 0)
    for (size_t i = 0; i < data.vehicles.size() && ride_idx >= 0; ++i)
    {
        data.vehicles[i].assigns.push_back(rides_list[ride_idx--]);
    }
}
