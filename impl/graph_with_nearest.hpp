#include "../libs/DataStructure.hpp"
#include "../libs/IO.hpp"
#include "../libs/Utils.hpp"
#include <algorithm>
#include <set>

void solve(Data& data)
{
    vector<int> rides;
    for (size_t i = 0; i < data.rides.size(); ++i)
        rides.push_back(i);

    vector<Position> v_pos(data.vehicles.size(), Position{0, 0});
    vector<int> v_time(v_pos.size(), 0);

    auto when_reached = [&](int v, int r) {
        const auto& ride = data.rides[r];
        int time = v_time[v];

        time += dist(v_pos[v], ride.from);
        if (time < ride.earlier_start)
        {
            time = ride.earlier_start;
        }

        return time + dist(ride.from, ride.to);
    };

    auto reached = [&](int v, int r) {
        const auto& ride = data.rides[r];

        if (v_time[v] + dist(v_pos[v], ride.from) > ride.latest_finish)
            return false;

        return when_reached(v, r) <= ride.latest_finish;

        /*time += dist(v_pos[v], ride.from);
        if (time < ride.earlier_start)
        {
            time = ride.earlier_start;
        }

        return time + dist(ride.from, ride.to) <= ride.latest_finish;
        */
    };
    while (!rides.empty())
    {
        //int i = rnd(0, data.vehicles.size() - 1);
        for (size_t i = 0; i < data.vehicles.size() && !rides.empty(); ++i)
        {
            sort(rides.begin(), rides.end(), [&](int l, int r) {
                if (reached(i, l) && reached(i, r))
                {
                    //return data.rides[l].earlier_start < data.rides[r].earlier_start;
                    return when_reached(i, l) < when_reached(i, r);
                    //return dist(v_pos[i], data.rides[l].from) < dist(v_pos[i], data.rides[r].from);
                    //return dist(data.rides[l].from, data.rides[l].to) > dist(data.rides[r].from, data.rides[r].to);
                }
                else
                {
                    return reached(i, l) > reached(i, r);
                }
            });

            static const double c_coef = 0.001;
            const int c_min_needed = std::min<int>(1, rides.size());

            int l = 0, r = rides.size() - 1;
            int searched = r;
            while (l <= r)
            {
                int mid = (l + r) >> 1;
                if (!reached(i, rides[mid]))
                {
                    searched = mid;
                    r = mid - 1;
                }
                else
                {
                    l = mid + 1;
                }
            }

            int to = max<int>(c_min_needed, c_coef * searched);

            if (to > 0)
            {
                r = min_element(rides.begin(), rides.begin() + to, [&](int x, int y) {
                    return dist(data.rides[x].from, data.rides[x].to) >
                           dist(data.rides[y].from, data.rides[y].to);
                }) - rides.begin();
                //r = rnd(0, to - 1);
                data.vehicles[i].assigns.push_back(rides[r]);
                v_time[i] = when_reached(i, rides[r]);
                v_pos[i] = data.rides[rides[r]].to;
                rides.erase(rides.begin() + r);
            }
            else
            {
                break;
            }

            if (rides.size() % 1000 == 0)
            cerr << rides.size() << " / " << data.rides.size() << endl;
        }
    }
}
