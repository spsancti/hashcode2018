#pragma once

#include <vector>

//#include "IO.hpp"
//#include "Utils.hpp"

using namespace std;

namespace {

struct Position
{
    int x;
    int y;
};

int dist(const Position& from, const Position& to);

struct Vehicle
{
    Position position;

    std::vector<int> assigns;
};

struct Ride
{
    Position from;
    Position to;
    int earlier_start;
    int latest_finish;

    int distance() { return dist(from, to); }
};

struct Grid
{
    int rows;
    int cols;
};

struct Data
{
    Grid grid;
    vector<Vehicle> vehicles;
    vector<Ride> rides;
    int bonus;
    int steps;
};

} // namespace
