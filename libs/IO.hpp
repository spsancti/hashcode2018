#pragma once

#include <iostream>
#include <cmath>
#include <algorithm>

#include "DataStructure.hpp"

namespace {

inline int dist(const Position& from, const Position& to)
{
    return std::abs(from.x - to.x) + std::abs(from.y - to.y);
}

inline Data readData()
{
    Data ret;

    int n, m, v, r, b, s;
    cin >> n >> m >> v >> r >> b >> s;

    ret.grid = Grid{n, m};
    ret.vehicles.resize(v, Vehicle{{0, 0}});
    ret.bonus = b;
    ret.steps = s;

    for (int i = 0; i < r; ++i)
    {
        Ride ride;
        cin >> ride.from.x >> ride.from.y >> ride.to.x >> ride.to.y >> ride.earlier_start >>
            ride.latest_finish;
        ret.rides.push_back(ride);
    }

    return ret;
}

inline int estimate(const Data& data)
{
    int final_score = 0;

    for (const auto& vehicle : data.vehicles)
    {
        Position pos{0, 0};
        int time = 0;
        int step_number = 0;

        for (int ride_id : vehicle.assigns)
        {
            if (++step_number > data.steps)
            {
                // we are done
                break;
            }

            const auto& ride = data.rides[ride_id];

            // move to the start point
            time += dist(pos, ride.from);
            pos = ride.from;
            
            if (time < ride.earlier_start)
            {
                // wait for the ride start time
                time = ride.earlier_start;
            }

            if (time == ride.earlier_start)
            {
                // oh, bonus !
                final_score += data.bonus;
            }

            time += dist(pos, ride.to);
            if (time <= ride.latest_finish)
            {
                final_score += dist(pos, ride.to);
            }
            pos = ride.to;
        }
    }
    return final_score;
}

inline void output(const Data& data)
{
    for (size_t i = 0; i < data.vehicles.size(); ++i)
    {
        cout << data.vehicles[i].assigns.size() << " ";
        for (int id : data.vehicles[i].assigns)
        {
            cout << id << " ";
        }
        cout << endl;
    }
}

}
