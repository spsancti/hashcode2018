#pragma once

#include <cstdlib>

int rnd(int l, int r)
{
    int x = int(rand()) | rand();
    return x % (r - l + 1) + l;
}
