#include <iostream>
#include <ctime>

#include "libs/DataStructure.hpp"
#include "libs/IO.hpp"
#include "impl/graph_with_nearest.hpp"

using namespace std;

int main()
{
    srand(time(0));
    auto data = readData();
    solve(data);
    output(data);
#ifdef WONDERBOLTS
    cerr << endl;
    cerr << "------------------------------------------" << endl;
    cerr << "TOTAL POINTS: " << estimate(data) << endl;
#endif
    return 0;
}
